package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class TestTest {
	TextAligner aligner = new TextAligner() {

		public String center(String text, int width) {
			int extra = (width - text.length()) / 2;
			return " ".repeat(extra) + text + " ".repeat(extra);
		}

		public String flushRight(String text, int width) {
      int extra = (width - text.length());
      return " ".repeat(extra) + text;
		}

		public String flushLeft(String text, int width) {
      int extra = width - text.length();
      return text + " ".repeat(extra);
		}

		public String justify(String text, int width) {
      String[]words = text.split(" ");
      int spacing = (width - String.join("", words).length()) / (words.length - 1);
      System.out.println(spacing);
      String justified = "";
      
      for(int i = 0; i < words.length - 1; i++){
        System.out.println(i);
        justified += words[i] + " ".repeat(spacing);
      }
      // Add last word
      justified += words[words.length - 1];

      // Add remaining space if any
      int spacingLeft = width - justified.length();
      justified += " ".repeat(spacingLeft);

      return justified;
		}};
		
	@Test
	void test() {
    // Flush right test
    assertEquals("    A", aligner.flushRight("A", 5));
    assertTrue(aligner.flushRight("B", 76).length() == 76);

    // Flush left test
    assertEquals("Aba    ",aligner.flushLeft("Aba", 7));
    assertTrue(aligner.flushLeft("BABA", 100).length() == 100);
    
    // Justify test
    // Expecting that justify will put the remaining spacing if any at the end of the String
    assertEquals("lars   lars   lars", aligner.justify("lars lars lars", 18));
    assertEquals("lars   lars   lars ", aligner.justify("lars lars lars", 19));
    assertTrue(aligner.justify("jdjd akaka djdje aleoiaf", 149).length() == 149);
	}

	@Test
	void testCenter() {
		assertEquals("  A  ", aligner.center("A", 5));
		assertEquals(" foo ", aligner.center("foo", 5));

	}
}
